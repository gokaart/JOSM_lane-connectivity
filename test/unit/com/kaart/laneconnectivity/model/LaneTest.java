// License: GPL. For details, see LICENSE file.
package com.kaart.laneconnectivity.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.openstreetmap.josm.TestUtils;
import org.openstreetmap.josm.data.coor.LatLon;
import org.openstreetmap.josm.data.osm.Node;
import org.openstreetmap.josm.data.osm.Way;
import org.openstreetmap.josm.testutils.annotations.BasicPreferences;

@BasicPreferences
class LaneTest {
    @Test
    void testGetRegularCount() {
        Way way = TestUtils.newWay("highway=residential lanes=2", new Node(new LatLon(0, 0)), new Node(new LatLon(0.1, 0.1)));
        assertEquals(1, Lane.getRegularCount(way, way.firstNode()));
        assertEquals(1, Lane.getRegularCount(way, way.lastNode()));
        
        way.put("lanes", "3");
        assertEquals(1, Lane.getRegularCount(way, way.firstNode()));
        assertEquals(2, Lane.getRegularCount(way, way.lastNode()));
        
        way.put("lanes:forward", "1");
        assertEquals(2, Lane.getRegularCount(way, way.firstNode()));
        assertEquals(1, Lane.getRegularCount(way, way.lastNode()));
        way.remove("lanes:forward");
        
        way.put("lanes:backward", "1");
        assertEquals(1, Lane.getRegularCount(way, way.firstNode()));
        assertEquals(2, Lane.getRegularCount(way, way.lastNode()));
        way.remove("lanes.backward");
        
        way.put("oneway", "yes");
        assertEquals(3, Lane.getRegularCount(way, way.lastNode()));
        assertEquals(0, Lane.getRegularCount(way, way.firstNode()));
        
        way.put("oneway", "-1");
        assertEquals(0, Lane.getRegularCount(way, way.lastNode()));
        assertEquals(3, Lane.getRegularCount(way, way.firstNode()));
    }
}
